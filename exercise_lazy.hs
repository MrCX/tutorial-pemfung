-- no. 1
plus = [ x+y | x <- [1 .. 4], y <- [2 .. 4], x > y ]

-- output berupa hasil penjumlahan x dan y, dengan x berisi himpunan nilai 1 sampai 4
-- dan y berisi himpunan nilai 2 sampai 4, jika x dan y memenuhi kondisi x > y maka x+y lalu,
-- dimasukkan kedalam list untuk menjadi output

--- >>> plus
--- [5,6,7]
---

-- no. 2
divisor n = [ x | x <- [1 .. n], n `mod` x == 0]

-- mod diharuskan memaka infix karena mod berada di tengah tengah yang di compare

--- >>> divisor 12
--- [1,2,3,4,6,12]
---

-- no. 3 quicksort

quicksort [] = []
quicksort (x:xs) = quicksort [ y | y <- xs, y <= x]
                    ++ [ x ] ++
                    quicksort [ y | y<-xs, y > x]

-- quicksort menggunkan pivot dimana pivot merupakan elemen pertama dari list yang di input
-- Lalu kita akan membelah list menjadi 2, yang lebih kecil dari pivot dan yang lebih besar dari pivot
-- Lalu dilakukan secara rekursif sampai mendapatkan list kosong, dimana di define diatas bahwa ketika
-- kondisi quicksort [] akan menghasilkan list kosong [] lalu di concat

--- >>> quicksort [3,1,4,2]
--- [1,2,3,4]
---

quicksort1 [] = []
quicksort1 (x:xs) = lowerlist ++[x]++ upperlist
                    where   lowerlist = quicksort1 [ y | y <- xs, y <= x]
                            upperlist = quicksort1 [ y | y <- xs, y > x]

--- >>> quicksort1 [3,1,4,2]
--- [1,2,3,4]
---

bubbleSort :: Ord a => [a] -> [a]
bubbleSort ts = foldl swapTill [] ts

swapTill [] x = [x]
swapTill (y:xs) x  = min x y : swapTill xs (max x y)

--- >>> bubbleSort [4,3,1,2]
--- [1,2,3,4]
---

bubbleSort' :: Ord a => [a] -> [a]
bubbleSort' = foldr swapTill' []

swapTill' x [] = [x]
swapTill' x (y:xs) = min x y : swapTill' (max x y) xs

-- no. 6
pythagoras1 = [ (x,y,z) | z <- [5 ..], y <- [4 ..z], x <- [3 .. y], x^2 + y^2 == z^2]

--- >>> take 5 pythagoras1
--- [(3,4,5),(6,8,10),(5,12,13),(9,12,15),(8,15,17)]
---
