-- no. 1
-- map (+3) list

map' [] = []
map' (x:xs) = [x+3] ++ map' xs 

--- >>> map' [1,2,3]
--- [4,5,6]
---

