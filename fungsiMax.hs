maxTiga :: Int -> Int -> Int -> Int
maxTiga x y z = maks (maks x y) z 
    where maks x y = if (x > y) then x
                        else y 

--- >>> maxTiga 5 8 2
--- 8
---